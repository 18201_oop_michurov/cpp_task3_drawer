#define CATCH_CONFIG_MAIN


#include "../catch.hpp"


#include "../src/shape/derived_shapes.hpp"
#include "../src/utility/utility_functions.hpp"
#include "../src/utility/shape_parser/parser.hpp"
#include "../src/bmp/bmp.hpp"


#define main _
#include "../src/main.cpp"
#undef main


TEST_CASE("test printNames variadic function")
{
    std::stringstream ss;

    auto r = Rectangle(10, 20);
    auto t = Triangle(0, 0, 10, 10);
    auto c1 = Circle(5);
    auto c2 = Circle(8);
    auto n = Named("Unknown");

    // arguments evaluation order is NOT specified
    // and hence output of, say,  print Names(Circle(1), Circle(2)) is undefined
    // and could be either "Circle.1 Circle.2" or "Circle.2 Circle.1", both are correct

    printNames(ss, r, t, c1, c2, n);
    printNames(ss, Named("CoolName"), Named("CoolerName"), Named("PassableName"));

    REQUIRE(ss.str() == "Rectangle.1 Triangle.1 Circle.1 Circle.2 Unknown.1\nCoolName.1 CoolerName.1 PassableName.1\n");
}


TEST_CASE("areas are computed correctly")
{
    SECTION("triangle")
    {
        SECTION("zero area")
        {
            Triangle t1(0, 0, 0, 0);

            REQUIRE(t1.area() == 0.0);

            Triangle t2(420, 69, 420, 69);

            REQUIRE(t2.area() == 0.0);
        }

        SECTION("positive area")
        {
            Triangle t1(100, 100, 200, 0);

            REQUIRE(t1.area() == Approx(10000));

            Triangle t2(-100, -100, -200, 0);

            REQUIRE(t2.area() == Approx(10000));
        }
    }

    SECTION("circle")
    {
        SECTION("zero area")
        {
            Circle c1(0);

            REQUIRE(c1.area() == 0.0);
        }

        SECTION("positive area")
        {
            Circle c1(10);

            REQUIRE(c1.area() == Approx(10 * 10 * 3.1415).epsilon(0.01));

            Circle c2(600);

            REQUIRE(c2.area() == Approx(600 * 600 * 3.1415).epsilon(0.1));
        }
    }

    SECTION("rectangle")
    {
        SECTION("zero area")
        {
            Rectangle r1(0, 0);

            REQUIRE(r1.area() == 0.0);

            Rectangle r2(1000, 0);

            REQUIRE(r2.area() == 0.0);

            Rectangle r3(0, 990);

            REQUIRE(r3.area() == 0.0);
        }

        SECTION("positive area")
        {
            Rectangle r1(1000, 1);

            REQUIRE(r1.area() == Approx(1000));

            Rectangle r2(40, 50);

            REQUIRE(r2.area() == Approx(2000));

            Rectangle r3(1, 1);

            REQUIRE(r3.area() == Approx(1));
        }
    }

    SECTION("weirdly named square() method")
    {
        Triangle t(6, 0, 0, 6);

        REQUIRE(t.square() == Approx(6 * 6 / 2));

        Shape * s = new Rectangle(10, 15);

        REQUIRE(s->square() == Approx(10 * 15));

        delete s;
    }
}


TEST_CASE("test totalArea variadic function")
{
    REQUIRE(totalArea(Rectangle(10, 5), Triangle(0, 6, 6, 6)) == Approx(68));

    REQUIRE(totalArea(Circle(10), Rectangle(150, 30), Triangle(-10, -10, 0, -80))
                == Approx(3.1415 * 100 + 150 * 30 + 80 * 5));
}


TEST_CASE("test bmp class")
{
    SECTION("bmp image generation")
    {
        SECTION("red balloon")
        {
            BMP bmp2(800, 600);
            std::ofstream fout("red_balloon.txt");
            fout << "800x600" << std::endl;

            for (int64_t x = 255; x >= 0; --x)
            {
                bmp2.fillShape(400, 50 + x * 1.2, Circle(x * 0.8, x, x * 0.3, x * 0.3));

                fout << "Circle (" << static_cast<uint32_t>(x * 0.8) << ") ["
                        << 400 << ", " << static_cast<uint32_t>(50 + x * 1.2) << "] {"
                        << static_cast<uint32_t>(x) << ", " << static_cast<uint32_t>(x * 0.3) << ", "
                        << static_cast<uint32_t>(x * 0.3) << "}" << std::endl;
            }

            bmp2.fillShape(400, 25, Rectangle(3, 51));
            fout << "Rectangle (3, 51) [400, 25] {black}" << std::endl;

            bmp2.fillShape(345, 450, Circle(25, 240, 240, 240));
            fout << "Circle (25) [345, 450] {240, 240, 240}" << std::endl;
            bmp2.fillShape(455, 450, Circle(25, 240, 240, 240));
            fout << "Circle (25) [455, 450] {240, 240, 240}" << std::endl;

            bmp2.fillShape(345, 450, Circle(15, 210, 80, 80));
            fout << "Circle (15) [345, 450] {210, 80, 80}" << std::endl;
            bmp2.fillShape(455, 450, Circle(15, 210, 80, 80));
            fout << "Circle (15) [455, 450] {210, 80, 80}" << std::endl;

            bmp2.fillShape(400, 430, Rectangle(40, 10, 240, 240, 240));
            fout << "Rectangle (40, 10) [400, 430] {240, 240, 240}" << std::endl;
            bmp2.fillShape(385, 450, Rectangle(10, 50, 240, 240, 240));
            fout << "Rectangle (10, 50) [385, 450] {240, 240, 240}" << std::endl;
            bmp2.fillShape(415, 450, Rectangle(10, 50, 240, 240, 240));
            fout << "Rectangle (10, 50) [415, 450] {240, 240, 240}" << std::endl;
            bmp2.fillShape(400, 450, Rectangle(10, 20, 240, 240, 240));
            fout << "Rectangle (10, 20) [400, 450] {240, 240, 240}" << std::endl;

            bmp2.write("red_balloon.bmp");
            fout.close();
        }

        SECTION("triangles")
        {
            BMP bmp2(800, 600);
            std::ofstream fout("triangles.txt");
            fout << "800x600" << std::endl;

            for (int64_t x = 255; x >= 0; --x)
            {
                bmp2.fillShape(
                        400 - x,
                        220 + static_cast<double>(127 - x) * 0.5,
                        Triangle(x, static_cast<double>(x + x) * 0.866, x + x, 0, x * 0.8, x * 0.7, x * 0.6)
                );

                fout << "Triangle (" << static_cast<int32_t>(x) << ", "
                     << static_cast<int32_t>(static_cast<double>(x + x) * 0.866) << ", "
                     << static_cast<int32_t>(x + x) << ", " << 0 << ") ["
                     << 400 - x << ", " << static_cast<uint32_t>(220 + static_cast<double>(127 - x) * 0.5) << "] {"
                     << static_cast<uint32_t>(x * 0.8) << ", " << static_cast<uint32_t>(x * 0.7) << ", "
                     << static_cast<uint32_t>(x * 0.6) << "}" << std::endl;
            }

            for (int64_t x = 255; x >= 0; --x)
            {
                bmp2.fillShape(
                        400 - x,
                        600 - (220 + static_cast<double>(127 - x) * 0.5),
                        Triangle(x, -1.0 * static_cast<double>(x + x) * 0.866, x + x, 0, x * 0.8, x * 0.7, x * 0.6)
                );

                fout << "Triangle (" << static_cast<int32_t>(x) << ", "
                     << static_cast<int32_t>(-1.0 * static_cast<double>(x + x) * 0.866) << ", "
                     << static_cast<int32_t>(x + x) << ", " << 0 << ") ["
                     << 400 - x << ", " << static_cast<uint32_t>(600 - (220 + static_cast<double>(127 - x) * 0.5))
                     << "] {" << static_cast<uint32_t>(x * 0.8) << ", " << static_cast<uint32_t>(x * 0.7) << ", "
                     << static_cast<uint32_t>(x * 0.6) << "}" << std::endl;
            }

            bmp2.write("triangles.bmp");
            fout.close();
        }

        SECTION("shape does not fit into canvas")
        {
            BMP bmp1(240, 320);
            std::ofstream fout("circle_part.txt");
            fout << "240x320" << std::endl;

            REQUIRE_NOTHROW(bmp1.fillShape(120, 165, Rectangle(250, 330, 200, 200, 255)));
            fout << "Rectangle (250, 330) [120, 165] {200, 200, 255}" << std::endl;

            REQUIRE_NOTHROW(bmp1.fillShape(-75, -55, Circle(200, 64, 127, 0)));
            fout << "Circle (200) [-75, -55] {64, 127, 0}" << std::endl;

            REQUIRE_NOTHROW(bmp1.fillShape(280, -500, Circle(600, 64, 127, 0)));
            fout << "Circle (600) [280, -500] {64, 127, 0}" << std::endl;

            REQUIRE_NOTHROW(bmp1.fillShape(250, 330, Circle(60, 255, 255, 0)));
            fout << "Circle (60) [250, 330] {255, 255, 0}" << std::endl;

            REQUIRE_NOTHROW(bmp1.write("circle_part.bmp"));
            fout.close();
        }
    }
}


TEST_CASE("test input parser")
{
    SECTION("shape data parser")
    {
        SECTION("empty input")
        {
            std::stringstream is;

            auto parser = shape_parser::parser(is);

            REQUIRE(not (parser.begin() != parser.end()));
        }

        SECTION("bad input")
        {
            SECTION("unknown shape")
            {
                std::stringstream is("Rectangle(10, 20) [50, 100] {green}\n"
                                     "Circle(8) [150, 10] {127, 127, 255}\n"
                                     "Metapod(10, 10, 10, 20) [250, 400] {255, 0, 0}");

                auto parser = shape_parser::parser(is);

                auto it = parser.begin();

                REQUIRE_NOTHROW(*it);

                ++it;

                REQUIRE_NOTHROW(*it);

                ++it;

                REQUIRE_THROWS_AS(*it, std::runtime_error const &);
            }

            SECTION("bad format")
            {
                SECTION("bad number of parameters")
                {
                    std::stringstream is("Rectangle(10, 20, 30) [50, 100] {green}");

                    auto parser = shape_parser::parser(is);

                    auto it = parser.begin();

                    REQUIRE_THROWS_AS(*it, std::runtime_error const &);
                }

                SECTION("bad numbers format")
                {
                    std::stringstream is("Rectangle(0b1010, 20) [50, 100] {green}");

                    auto parser = shape_parser::parser(is);

                    auto it = parser.begin();

                    REQUIRE_THROWS_AS(*it, std::runtime_error const &);
                }

                SECTION("bad colour name")
                {
                    std::stringstream is("Rectangle(0b1010, 20) [50, 100] {colour of the night}");

                    auto parser = shape_parser::parser(is);

                    auto it = parser.begin();

                    REQUIRE_THROWS_AS(*it, std::runtime_error const &);
                }

                SECTION("extra characters in between parentheses blocks")
                {
                    std::stringstream is("Rectangle(10, 20)_~!@#$%^&*()_+[50, 100]abcde i hate my life{red}");

                    auto parser = shape_parser::parser(is);

                    auto it = parser.begin();

                    REQUIRE_THROWS_AS(*it, std::runtime_error const &);
                }
            }
        }

        SECTION("normal input")
        {
            std::stringstream is(" Rectangle(10,20) [50, 100] {green}\n"
                                 "Circle(8) [150,   10] { 127, 127, 255 } \n"
                                 "triangle(10, 10, 10, 20)[250, 400]{255, 0, 0}");

            auto parser = shape_parser::parser(is);

            std::vector<ShapeContainer> container_vector(parser.begin(), parser.end());

            REQUIRE(container_vector[0].getShape().getName().rfind("Rectangle") == 0);

            REQUIRE(container_vector[0].getShape().blue == 0);
            REQUIRE(container_vector[0].getShape().green == 128);
            REQUIRE(container_vector[0].getShape().red == 0);

            REQUIRE(container_vector[0].x0 == 50);
            REQUIRE(container_vector[0].y0 == 100);

            REQUIRE(container_vector[1].getShape().getName().rfind("Circle") == 0);

            REQUIRE(container_vector[1].getShape().blue == 255);
            REQUIRE(container_vector[1].getShape().green == 127);
            REQUIRE(container_vector[1].getShape().red == 127);

            REQUIRE(container_vector[1].x0 == 150);
            REQUIRE(container_vector[1].y0 == 10);

            REQUIRE(container_vector[2].getShape().getName().rfind("Triangle") == 0);

            REQUIRE(container_vector[2].getShape().blue == 0);
            REQUIRE(container_vector[2].getShape().green == 0);
            REQUIRE(container_vector[2].getShape().red == 255);

            REQUIRE(container_vector[2].x0 == 250);
            REQUIRE(container_vector[2].y0 == 400);

            REQUIRE(container_vector.size() == 3);
        }
    }

    SECTION("resolution parser")
    {
        SECTION("empty string")
        {
            REQUIRE_THROWS(shape_parser::getImageResolution(""));
        }

        SECTION("bad format")
        {
            REQUIRE_THROWS(shape_parser::getImageResolution("abcxdef"));
            REQUIRE_THROWS(shape_parser::getImageResolution("100x"));
            REQUIRE_THROWS(shape_parser::getImageResolution("x"));
            REQUIRE_THROWS(shape_parser::getImageResolution("-3x3"));
        }

        SECTION("normal input")
        {
            auto p1 = std::pair<uint32_t, uint32_t>(100, 200);
            auto p2 = std::pair<uint32_t, uint32_t>(0, 0);
            auto p3 = std::pair<uint32_t, uint32_t>(999999999, 999999999);

            REQUIRE(shape_parser::getImageResolution("100x200") == p1);
            REQUIRE(shape_parser::getImageResolution("0 X 0") == p2);
            REQUIRE(shape_parser::getImageResolution("    0000999999999  \t  X\t 00999999999     ") == p3);
        }
    }
}


TEST_CASE("test do_main")
{
    SECTION("pixels")
    {
        char const * argv[] = { nullptr, "in.txt", "-o", "pixels.bmp" };

        optind = 1;

        std::ofstream os("in.txt");

        os << "9x9\n"
              "Rectangle(3, 3) [1, 1] {magenta}\n"
              "Rectangle(3, 3) [4, 4] {magenta}\n"
              "Rectangle(3, 3) [7, 7] {magenta}\n"
              "Rectangle(3, 3) [1, 4] {silver}\n"
              "Rectangle(3, 3) [4, 1] {silver}\n"
              "Rectangle(3, 3) [4, 7] {silver}\n"
              "Rectangle(3, 3) [7, 4] {silver}\n"
              "Rectangle(1, 1) [0, 6] {black}\n"
              "Rectangle(1, 1) [1, 7] {black}\n"
              "Rectangle(1, 1) [2, 8] {black}\n"
              "Rectangle(1, 1) [0, 8] {black}\n"
              "Rectangle(1, 1) [2, 6] {black}\n"
              "Rectangle(1, 1) [0, 7] {white}\n"
              "Rectangle(1, 1) [1, 6] {white}\n"
              "Rectangle(1, 1) [1, 8] {white}\n"
              "Rectangle(1, 1) [2, 7] {white}\n"
              "Rectangle(1, 1) [6, 0] {cyan}\n"
              "Rectangle(1, 1) [7, 1] {cyan}\n"
              "Rectangle(1, 1) [8, 2] {cyan}\n"
              "Rectangle(1, 1) [8, 0] {cyan}\n"
              "Rectangle(1, 1) [6, 2] {cyan}\n"
              "Rectangle(1, 1) [7, 0] {navy}\n"
              "Rectangle(1, 1) [6, 1] {navy}\n"
              "Rectangle(1, 1) [8, 1] {navy}\n"
              "Rectangle(1, 1) [7, 2] {navy}\n"
              "Circle(1) [4, 4] {maroon}\n"
              "Triangle(2, 2, 2, 0) [6, 6] {yellow}";

        os.close();

        std::stringstream iss;
        std::stringstream oss;

        do_main(iss, oss, 4, const_cast<char **>(argv));

        REQUIRE(oss.str().empty());
    }

    SECTION("empty lines")
    {
        char const * argv[] = { nullptr, "-o", "grid" };

        optind = 1;

        std::stringstream iss("10x10\n\n"
                              "Rectangle(5, 5) [2, 2] {silver}\n\n"
                              "Rectangle(5, 5) [7, 2] {white}\n\n"
                              "Rectangle(5, 5) [2, 7] {white}\n\n"
                              "Rectangle(5, 5) [7, 7] {silver}\n\n"
                              "");
        std::stringstream oss;

        do_main(iss, oss, 3, const_cast<char **>(argv));

        REQUIRE(oss.str().empty());
    }
}
