# Drawer

Generates a .bmp image as specified [here](https://avoronkov.gitlab.io/oop/18201.cpp/task3/).

### Build 

```bash
    $ mkdir build
    $ cd build
    $ cmake .. && make
```

### Usage

    $ drawer [input_file] [[-o] output_file]
    
### Allowed colour names

* Black
* White
* Red
* Lime
* Blue
* Yellow
* Cyan
* Magenta / Fuchsia
* Silver
* Gray
* Maroon
* Olive
* Green
* Purple
* Teal
* Navy

[Source](https://www.rapidtables.com/web/color/RGB_Color.html)
