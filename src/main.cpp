#include <iostream>
#include <fstream>


#include "shape/derived_shapes.hpp"
#include "bmp/bmp.hpp"


#include "utility/shape_parser/parser.hpp"
#include "utility/arg_parser/parser.hpp"


inline bool endsWith(
        std::string const & string,
        std::string const & suffix)
{
    if (string.length() >= suffix.length())
    {
        return (0 == string.compare(string.length() - suffix.length(), suffix.length(), suffix));
    }
    else
    {
        return false;
    }
}


inline int do_main(
        std::istream & in,
        std::ostream & out,
        int argc,
        char * argv[])
{
    GlobalArgs_t args;

    try
    {
        args = arg_parser::parse_arguments(argc, argv, ":o:");
    }
    catch (std::runtime_error const & e)
    {
        out << e.what() << std::endl;

        return 1;
    }

    if (args.output_file.empty())
    {
        if (argc == 3 and args.input_file == argv[1])
        {
            args.output_file = argv[2];
        }
        else
        {
            args.output_file = "a.bmp";
        }
    }
    else
    {
        if (not endsWith(args.output_file, ".bmp"))
        {
            args.output_file.append(".bmp");
        }
    }

    std::ifstream ifs;

    if (not args.input_file.empty())
    {
        ifs.open(args.input_file);

        if (ifs.fail())
        {
            out << "Error: что-то у меня не получилось открыть файл \"" << args.input_file
                << "\". Может, попробуете еще раз?" << std::endl;

            return 1;
        }
    }

    std::istream & is = ifs.is_open() ? ifs : in;

    std::string buf;

    std::getline(is, buf);

    try
    {
        auto p = shape_parser::getImageResolution(buf);

        BMP image(p.first, p.second);

        for (auto const && container : shape_parser::parser(is))
        {
            image.fillShape(container.x0, container.y0, container.getShape());
        }

        image.write(args.output_file);
    }
    catch (std::runtime_error const & e)
    {
        std::cout << e.what() << std::endl;

        return 1;
    }

    return 0;
}


int main(
        int argc,
        char * argv[])
{
    return do_main(std::cin, std::cout, argc, argv);
}