#include "colored.hpp"


Colored::Colored(
        uint8_t red,
        uint8_t green,
        uint8_t blue)
        :
        blue(blue),
        green(green),
        red(red)
{}


Colored::Colored()
        :
        blue(0),
        green(0),
        red(0)
{}
