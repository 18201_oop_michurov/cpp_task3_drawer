#ifndef DRAWER_COLORED_HPP
#define DRAWER_COLORED_HPP


#include <stdint-gcc.h>

class Colored
{
public:
    uint8_t blue;
    uint8_t green;
    uint8_t red;

public:
    Colored();

    ~Colored() = default;

    Colored(Colored const & other) = default;
    Colored(Colored && other) noexcept = default;

    Colored & operator=(Colored const & other) = default;
    Colored & operator=(Colored && other) noexcept = default;

    Colored(
            uint8_t red,
            uint8_t green,
            uint8_t blue);
};


#endif //DRAWER_COLORED_HPP
