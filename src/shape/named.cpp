#include "named.hpp"


std::map<std::string, size_t> Named::name_count = {};


std::string Named::makeUniqueId(
        std::string const & name)
{
    return name + '.' + std::to_string(++Named::name_count[name]);
}


Named::Named() 
        : 
        name(Named::makeUniqueId("Named"))
{}

Named::Named(
        std::string const & name) 
        : 
        name(Named::makeUniqueId(name))
{}


std::string Named::getName() const
{
    return this->name;
}
