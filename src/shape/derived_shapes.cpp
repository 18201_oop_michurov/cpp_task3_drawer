#include <algorithm>
#include <cmath>
#include <iostream>


#include "derived_shapes.hpp"


Triangle::Triangle(
        int32_t dx1,
        int32_t dy1,
        int32_t dx2,
        int32_t dy2)
        :
        dx1(dx1),
        dy1(dy1),
        dx2(dx2),
        dy2(dy2),
        Named("Triangle")
{}


Triangle::Triangle(
        int32_t dx1,
        int32_t dy1,
        int32_t dx2,
        int32_t dy2,
        Colored colour)
        :
        dx1(dx1),
        dy1(dy1),
        dx2(dx2),
        dy2(dy2),
        Named("Triangle"),
        Colored(colour)
{
    // std::cout << "Triangle (" << dx1 << ", " << dy1 << ", " << dx2 << ", " << dy2 << ")" << std::endl;
}


Triangle::Triangle(
        int32_t dx1,
        int32_t dy1,
        int32_t dx2,
        int32_t dy2,
        uint8_t red,
        uint8_t green,
        uint8_t blue)
        :
        dx1(dx1),
        dy1(dy1),
        dx2(dx2),
        dy2(dy2),
        Named("Triangle"),
        Colored(red, green, blue)
{
    // std::cout << "Triangle (" << dx1 << ", " << dy1 << ", " << dx2 << ", " << dy2 << ")" << std::endl;
}


void Triangle::fillTopFlatTriangle(
        std::pair<int32_t, int32_t> const & v1,
        std::pair<int32_t, int32_t> const & v2,
        std::pair<int32_t, int32_t> const & v3,
        std::vector<Shape::Segment> & segments)
{
    double inv_slope1 = static_cast<double>(v3.first - v1.first) / (v3.second - v1.second);
    double inv_slope2 = static_cast<double>(v3.first - v2.first) / (v3.second - v2.second);

    double cur_x1 = v3.first;
    double cur_x2 = v3.first;

    for (int64_t scan_line_y = v3.second; scan_line_y < v1.second; ++scan_line_y)
    {
        int64_t x1 = cur_x1 < cur_x2 ? std::llround(cur_x1) : std::llround(cur_x2);
        int64_t x2 = cur_x1 >= cur_x2 ? std::llround(cur_x1) : std::llround(cur_x2);

        segments.emplace_back(x1, x2, scan_line_y);

        cur_x1 += inv_slope1;
        cur_x2 += inv_slope2;
    }
}


void Triangle::fillBottomFlatTriangle(
        std::pair<int32_t, int32_t> const & v1,
        std::pair<int32_t, int32_t> const & v2,
        std::pair<int32_t, int32_t> const & v3,
        std::vector<Shape::Segment> & segments)
{
    double inv_slope1 = static_cast<double>(v2.first - v1.first) / (v2.second - v1.second);
    double inv_slope2 = static_cast<double>(v3.first - v1.first) / (v3.second - v1.second);

    double cur_x1 = v1.first;
    double cur_x2 = v1.first;

    for (int64_t scan_line_y = v1.second; scan_line_y >= v3.second; --scan_line_y)
    {
        int64_t x1 = cur_x1 < cur_x2 ? std::llround(cur_x1) : std::llround(cur_x2);
        int64_t x2 = cur_x1 >= cur_x2 ? std::llround(cur_x1) : std::llround(cur_x2);

        segments.emplace_back(x1, x2, scan_line_y);

        cur_x1 -= inv_slope1;
        cur_x2 -= inv_slope2;
    }
}


double Triangle::area() const
{
    double a = std::sqrt(dx1 * dx1 + dy1 * dy1);
    double b = std::sqrt(dx2 * dx2 + dy2 * dy2);
    double c = std::sqrt((dx2 - dx1) * (dx2 - dx1) + (dy2 - dy1) * (dy2 - dy1));
    double p = (a + b + c) / 2;

    return std::sqrt(p * (p - a) * (p - b) * (p - c));
}


std::vector<Shape::Segment> Triangle::getSegments() const
{
    std::vector<Shape::Segment> segments;

    if (this->area() > 0.0)
    {
        auto cmp = [](std::pair<int32_t, int32_t> const & p1, std::pair<int32_t, int32_t> const & p2)
        {
            return p1.second > p2.second;
        };

        auto origin = std::pair<int32_t, int32_t>(0, 0);

        std::vector<std::pair<int32_t, int32_t>> vertices = {
                {this->dx2, this->dy2},
                {this->dx1, this->dy1},
                origin
        };

        std::sort(vertices.begin(), vertices.end(), cmp);

        auto const & top = vertices[0];
        auto const & bottom = vertices[2];
        auto const & middle = vertices[1];

        segments.reserve(top.second - bottom.second + 1);

        if (middle.second == bottom.second)
        {
            Triangle::fillBottomFlatTriangle(top, middle, bottom, segments);
        }
        else if (middle.second == top.second)
        {
            Triangle::fillTopFlatTriangle(top, middle, bottom, segments);
        }
        else
        {
            auto v4 = std::pair<int32_t, int32_t>(
                    std::llround(
                            top.first
                            + (static_cast<double>(middle.second - top.second) / (bottom.second - top.second))
                            * (bottom.first - top.first)
                    ),
                    middle.second
            );

            Triangle::fillBottomFlatTriangle(top, middle, v4, segments);
            Triangle::fillTopFlatTriangle(middle, v4, bottom, segments);
        }
    }

    return segments;
}


Circle::Circle(
        uint32_t r)
        :
        r(r),
        Named("Circle")
{}


Circle::Circle(
        uint32_t r,
        Colored colour)
        :
        r(r),
        Named("Circle"),
        Colored(colour)
{}


Circle::Circle(
        uint32_t r,
        uint8_t red,
        uint8_t green,
        uint8_t blue)
        :
        r(r),
        Named("Circle"),
        Colored(red, green, blue)
{}


double Circle::area() const
{
    return std::acos(-1) * this->r * this->r;
}


std::vector<Shape::Segment> Circle::getSegments() const
{
    std::vector<Shape::Segment> segments;

    if (this->r > 0)
    {
        segments.reserve(2 * this->r + 1);

        segments.emplace_back(-static_cast<int64_t>(this->r), this->r, 0);

        for (int64_t y = 1; y <= this->r; ++y)
        {
            for (int64_t x = this->r; x >= this->r - y; --x)
            {
                if (x * x + y * y <= this->r * this->r + this->r / 2)
                {
                    segments.emplace_back(-x,  x, y);
                    segments.emplace_back(-x,  x, -y);

                    break;
                }
            }
        }
    }

    return segments;
}


Rectangle::Rectangle(
        uint32_t w,
        uint32_t h)
        :
        h(h),
        w(w),
        Named("Rectangle")
{}


Rectangle::Rectangle(
        uint32_t w,
        uint32_t h,
        Colored colour)
        :
        h(h),
        w(w),
        Named("Rectangle"),
        Colored(colour)
{}


Rectangle::Rectangle(
        uint32_t w,
        uint32_t h,
        uint8_t red,
        uint8_t green,
        uint8_t blue)
        :
        h(h),
        w(w),
        Named("Rectangle"),
        Colored(red, green, blue)
{}


double Rectangle::area() const
{
    return this->w * this->h;
}


std::vector<Shape::Segment> Rectangle::getSegments() const
{
    std::vector<Shape::Segment> segments;

    if (this->h > 0 and this->w > 0)
    {
        segments.resize(this->h);

        for (int64_t y = 1; y <= this->h; ++y)
        {
            int64_t x0 = -static_cast<int64_t>(this->w) / 2;

            segments.emplace_back(x0, x0 + this->w - 1, (this->h + 1) / 2 - y);
        }
    }

    return segments;
}
