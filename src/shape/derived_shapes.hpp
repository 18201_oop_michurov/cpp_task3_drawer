#ifndef DRAWER_DERIVED_SHAPES_HPP
#define DRAWER_DERIVED_SHAPES_HPP


#include "shape.hpp"
#include "named.hpp"
#include "colored.hpp"


class Triangle :
        public virtual Shape
{
private:
    static void fillTopFlatTriangle(
            std::pair<int32_t, int32_t> const & v1,
            std::pair<int32_t, int32_t> const & v2,
            std::pair<int32_t, int32_t> const & v3,
            std::vector<Shape::Segment> & segments);

    static void fillBottomFlatTriangle(
            std::pair<int32_t, int32_t> const & v1,
            std::pair<int32_t, int32_t> const & v2,
            std::pair<int32_t, int32_t> const & v3,
            std::vector<Shape::Segment> & segments);

public:
    Triangle() = delete;

    Triangle(
            int32_t dx1,
            int32_t dy1,
            int32_t dx2,
            int32_t dy2);

    Triangle(
            int32_t dx1,
            int32_t dy1,
            int32_t dx2,
            int32_t dy2,
            Colored colour);

    Triangle(
            int32_t dx1,
            int32_t dy1,
            int32_t dx2,
            int32_t dy2,
            uint8_t red,
            uint8_t green,
            uint8_t blue);

    Triangle(Triangle const & other) = default;

    ~Triangle() override = default;

    double area() const override;

    std::vector<Shape::Segment> getSegments() const override;

    Triangle & operator=(Triangle const & other) = default;

private:
    int32_t dx1;
    int32_t dy1;
    int32_t dx2;
    int32_t dy2;
};


class Circle :
        public virtual Shape
{
public:
    Circle() = delete;

    explicit Circle(uint32_t r);

    Circle(
            uint32_t r,
            Colored colour);

    Circle(
            uint32_t r,
            uint8_t red,
            uint8_t green,
            uint8_t blue);

    Circle(Circle const & other) = default;

    ~Circle() override = default;

    double area() const override;

    std::vector<Shape::Segment> getSegments() const override;

    Circle & operator=(Circle const & other) = default;

private:
    uint32_t r;
};


class Rectangle :
        public virtual Shape
{
public:
    Rectangle() = delete;

    Rectangle(
            uint32_t w,
            uint32_t h);

    Rectangle(
            uint32_t w,
            uint32_t h,
            Colored colour);

    Rectangle(
            uint32_t w,
            uint32_t h,
            uint8_t red,
            uint8_t green,
            uint8_t blue);

    Rectangle(Rectangle const & other) = default;

    ~Rectangle() override = default;

    double area() const override;

    std::vector<Shape::Segment> getSegments() const override;

    Rectangle & operator=(Rectangle const & other) = default;

private:
    uint32_t w;
    uint32_t h;
};


#endif //DRAWER_DERIVED_SHAPES_HPP
