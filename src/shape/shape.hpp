#ifndef DRAWER_SHAPE_HPP
#define DRAWER_SHAPE_HPP


#include <vector>


#include "named.hpp"
#include "colored.hpp"


class Shape :
        public virtual Named,
        public virtual Colored
{
public:
    struct Segment
    {
        Segment() = default;

        Segment(
                int64_t x0,
                int64_t x1,
                int64_t y)
                :
                x0(x0),
                x1(x1),
                y(y)
        {}

        int64_t x0;
        int64_t x1;
        int64_t y;
    };

public:
    virtual double area() const = 0;
    
    virtual double square() const
    {
        return this->area();
    }

    virtual std::vector<Shape::Segment> getSegments() const = 0;

    virtual ~Shape() = default;
};


#endif //DRAWER_SHAPE_HPP
