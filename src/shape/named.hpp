#ifndef DRAWER_NAMED_HPP
#define DRAWER_NAMED_HPP


#include <string>
#include <map>


class Named
{
private:
    static std::map<std::string, size_t> name_count;

    static std::string makeUniqueId(std::string const & name);

public:
    Named();

    explicit Named(std::string const & name);

    Named(Named const & other) = default;

    ~Named() = default;

    std::string getName() const;

    Named & operator=(Named const& other) = default;

private:
    std::string name;
};


#endif //DRAWER_NAMED_HPP
