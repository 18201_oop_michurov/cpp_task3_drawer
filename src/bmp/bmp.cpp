#include <fstream>


#ifdef DEBUG_OUTPUT_SHAPE_NAME
#include <iostream>
#endif


#include "bmp.hpp"


inline uint32_t BMP::makeStrideAligned(
        uint32_t align_stride) const
{
    uint32_t new_stride = this->row_stride;

    while (new_stride % align_stride != 0)
    {
        new_stride++;
    }

    return new_stride;
}

inline void BMP::plotPixel(
        uint32_t x,
        uint32_t y,
        uint8_t B,
        uint8_t G,
        uint8_t R)
{
    this->data[channels * (y * this->bmp_info_header.width + x) + 0] = B;
    this->data[channels * (y * this->bmp_info_header.width + x) + 1] = G;
    this->data[channels * (y * this->bmp_info_header.width + x) + 2] = R;
}

inline void BMP::plotHorizontalLine(
        int64_t x0,
        int64_t x1,
        int64_t y,
        uint8_t B,
        uint8_t G,
        uint8_t R)
{
    if (x0 > x1)
    {
        std::swap(x0, x1);
    }

    if (y >= this->bmp_info_header.height or y < 0 or x1 < 0 or x0 >= this->bmp_info_header.width)
    {
        return;
    }

    x0 = x0 >= 0 ? x0 : 0;
    x1 = x1 < this->bmp_info_header.width ? x1 : this->bmp_info_header.width - 1;

    for (auto x = x0; x <= x1; ++x)
    {
        this->plotPixel(x, y, B, G, R);
    }
}


BMP::BMP(
        int32_t width,
        int32_t height)
        :
        row_stride(width * 3)
{
    if (width <= 0 or height <= 0)
    {
        throw std::runtime_error("The image width and height must be positive numbers.");
    }

    this->bmp_info_header.width = width;
    this->bmp_info_header.height = height;

    this->bmp_info_header.size = sizeof(BMPInfoHeader);
    this->file_header.offset_data = sizeof(BMPFileHeader) + sizeof(BMPInfoHeader);

    this->data.resize(this->row_stride * height, 255);

    uint32_t new_stride = this->makeStrideAligned(4);

    this->file_header.file_size = this->file_header.offset_data + this->data.size()
                                  + this->bmp_info_header.height * (new_stride - this->row_stride);
}


void BMP::write(std::string const & file_name) const
{
    std::ofstream of(file_name, std::ios_base::binary);

    if (of.is_open())
    {
        of.write((const char*)&file_header, sizeof(file_header));
        of.write((const char*)&bmp_info_header, sizeof(bmp_info_header));

        if (this->bmp_info_header.width % 4 == 0)
        {
            of.write((const char*)data.data(), data.size());
        }
        else
        {
            uint32_t new_stride = this->makeStrideAligned(4);
            std::vector<uint8_t> padding_row(new_stride - this->row_stride, 255);

            for (int y = 0; y < this->bmp_info_header.height; ++y)
            {
                of.write((const char*)(this->data.data() + this->row_stride * y), this->row_stride);
                of.write((const char*)padding_row.data(), padding_row.size());
            }
        }
    }
    else
    {
        throw std::runtime_error("Unable to open the output image file.");
    }
}


void BMP::fillShape(
        int64_t x0,
        int64_t y0,
        Shape const & shape)
{
#ifdef DEBUG_OUTPUT_SHAPE_NAME
    std::cout << "Drawing " << shape.getName() << std::endl;
#endif
    for (Shape::Segment const & segment : shape.getSegments())
    {
        this->plotHorizontalLine(
                x0 + segment.x0,
                x0 + segment.x1,
                y0 + segment.y,
                shape.blue,
                shape.green,
                shape.red);
    }
}
