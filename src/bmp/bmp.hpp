#ifndef CPP_TASK3_DRAWER_BMP_HPP
#define CPP_TASK3_DRAWER_BMP_HPP


/*                                                               */
/* Mostly copied from https://github.com/sol-prog/cpp-bmp-images */
/*                                                               */


#include <vector>


#include "../shape/shape.hpp"


#pragma pack(push, 1)
struct BMPFileHeader
{
    uint16_t file_type = 0x4D42;
    uint32_t file_size = 0;
    uint16_t reserved1 = 0;
    uint16_t reserved2 = 0;
    uint32_t offset_data = 0;
};


struct BMPInfoHeader
{
    uint32_t size = 0;
    int32_t width = 0;
    int32_t height = 0;
    uint16_t planes = 1;
    uint16_t bit_count = 24;
    uint32_t compression = 0;
    uint32_t size_image = 0;
    int32_t x_pixels_per_meter = 0;
    int32_t y_pixels_per_meter = 0;
    uint32_t colors_used = 0;
    uint32_t colors_important = 0;
};
#pragma pack(pop)


class BMP
{
private:
    static uint32_t const channels = 3;

    inline uint32_t makeStrideAligned(
            uint32_t align_stride) const;

    inline void plotPixel(
            uint32_t x,
            uint32_t y,
            uint8_t B,
            uint8_t G,
            uint8_t R);

    inline void plotHorizontalLine(
            int64_t x0,
            int64_t x1,
            int64_t y,
            uint8_t B,
            uint8_t G,
            uint8_t R);

public:
    BMP(
            int32_t width,
            int32_t height);

    void write(std::string const & file_name) const;

    void fillShape(
            int64_t x0,
            int64_t y0,
            Shape const & shape);

private:
    uint32_t row_stride;

    BMPFileHeader file_header;
    BMPInfoHeader bmp_info_header;

    std::vector<uint8_t> data;
};


#endif //CPP_TASK3_DRAWER_BMP_HPP
