#include <algorithm>
#include <unordered_map>
#include <regex>


// #define DEBUG_OUTPUT_REGEX


#ifdef DEBUG_OUTPUT_READLINE
#include <iostream>
#endif

#ifdef DEBUG_OUTPUT_REGEX
#include <iostream>
#endif


#include "parser.hpp"
#include "../../shape/derived_shapes.hpp"


using namespace shape_parser;


template <typename IntegralType>
IntegralType shape_parser::convert(std::string const & string)
{
    long long int val;

    std::stringstream err;

    try
    {
        val = std::stoll(string, nullptr);
    }
    catch (std::out_of_range & e)
    {
        err << "Converted value " << string << " exceeds allowed range ["
            << static_cast<long long int>(std::numeric_limits<IntegralType>::min())
            << ", " << static_cast<long long int>(std::numeric_limits<IntegralType>::max()) << "]" << std::endl;

        throw std::out_of_range(err.str());
    }

    if (val > std::numeric_limits<IntegralType>::max() or val < std::numeric_limits<IntegralType>::min())
    {
        err << "Converted value " << val << " exceeds allowed range ["
            << static_cast<long long int>(std::numeric_limits<IntegralType>::min())
            << ", " << static_cast<long long int>(std::numeric_limits<IntegralType>::max()) << "]" << std::endl;

        throw std::out_of_range(err.str());
    }


    return static_cast<IntegralType>(val);
}


std::pair<uint32_t, uint32_t> shape_parser::getImageResolution(
        std::string const & string)
{
    std::regex resolution_regex(R"=(^\s*(\d*)\s*[xXхХ]\s*(\d*)\s*$)=");

    std::cmatch match;

    if (std::regex_match(string.c_str(), match, resolution_regex))
    {
        try
        {
            return {convert<uint32_t>(match[1].str()), convert<uint32_t>(match[2].str())};
        }
        catch (std::out_of_range & e)
        {
            throw std::runtime_error("Error: resolution data is not properly formatted:\n" + string + "\n" + e.what());
        }
    }

    throw std::runtime_error("Error: resolution data is not properly formatted:\n" + string);
}


Colored shape_parser::getColourFromString(
        std::string const & string)
{
    std::string colour_name(string);

    std::transform(
            colour_name.begin(),
            colour_name.end(),
            colour_name.begin(),
            [](char const c) -> char
            {
                return static_cast<char>(std::tolower(c));
            }
    );

    static std::unordered_map<std::string, Colored> colours = {
            {"black", {0, 0, 0}},
            {"white", {255, 255, 255}},
            {"red", {255, 0, 0}},
            {"lime", {0, 255, 0}},
            {"blue", {0, 0, 255}},
            {"yellow", {255, 255, 0}},
            {"cyan", {0, 255, 255}},
            {"aqua", {0, 255, 255}},
            {"magenta", {255, 0, 255}},
            {"fuchsia", {255, 0, 255}},
            {"silver", {192, 192, 192}},
            {"gray", {128, 128, 128}},
            {"maroon", {128, 0, 0}},
            {"olive", {128, 128, 0}},
            {"green", {0, 128, 0}},
            {"purple", {128, 0, 128}},
            {"teal", {0, 128, 128}},
            {"navy", {0, 0, 128}}
    };

    auto it = colours.find(colour_name);

    if (it == colours.end())
    {
        throw std::runtime_error("Error: colour data is not properly formatted:\n" + string);
    }

    return it->second;
}


/*--------------------------------------------------------------------------------------------------------------------*/
/*                                                    Parser                                                          */
/*--------------------------------------------------------------------------------------------------------------------*/


parser::parser(
        std::istream & input)
        :
        is(input)
{}


/*--------------------------------------------------------------------------------------------------------------------*/
/*                                                   Iterator                                                         */
/*--------------------------------------------------------------------------------------------------------------------*/


parser::iterator parser::begin()
{
    return iterator(this->is, false);
}


parser::iterator parser::end()
{
    return iterator(this->is, true);
}


parser::iterator::iterator(
        std::istream & input,
        bool eof)
        :
        is(input),
        eof(eof)
{
    if (not eof)
    {
        ++(*this);

        this->eof = this->is.eof() and this->buf.empty();
    }
}


parser::iterator & parser::iterator::operator++()
{
    this->eof = this->is.eof();

    do
    {
        std::getline(this->is, this->buf);

#ifdef DEBUG_OUTPUT_READLINE
        std::cout << "Line: \"" << this->buf << "\"" << std::endl;
        std::cout  << (this->eof ? "EOF" : "") << std::endl;
#endif
    }
    while (this->buf.empty() and not this->is.eof());

    this->eof = this->eof or (this->is.eof() and this->buf.empty());

    return *this;
}


bool parser::iterator::operator!=(
        parser::iterator const & other) const
{
    return this->is.rdbuf() != other.is.rdbuf() or this->eof != other.eof;
}


ShapeContainer parser::iterator::operator*() const
{
    std::string shape_description(this->buf);

    shape_description.erase(
            std::remove_if(
                    shape_description.begin(),
                    shape_description.end(),
                    [](char const c) -> bool
                    {
                        return std::isspace(c);
                    }
            ),
            shape_description.end()
    );

    std::regex const circle_regex(
            R"=(^Circle\((\d*)\)\[(-?\d*),(-?\d*)\]\{(?:(\d*),(\d*),(\d*)|(\w*))\}$)=", std::regex_constants::icase);
    std::regex const triangle_regex(
            R"=(^Triangle\((-?\d*),(-?\d*),(-?\d*),(-?\d*)\)\[(-?\d*),(-?\d*)\]\{(?:(\d*),(\d*),(\d*)|(\w*))\}$)=",
            std::regex_constants::icase);
    std::regex const rectangle_regex(
            R"=(^Rectangle\((\d*),(\d*)\)\[(\d*),(\d*)\]\{(?:(\d*),(\d*),(\d*)|(\w*))\}$)=",
            std::regex_constants::icase);

    std::cmatch match;

    if (std::regex_match(shape_description.c_str(), match, circle_regex))
    {
        try
        {
            auto radius = convert<uint32_t>(match[1].str());
            auto origin_x = convert<int32_t>(match[2].str());
            auto origin_y = convert<int32_t>(match[3].str());

            Colored colour;

            if (match[4].matched)
            {
                colour.red = convert<uint8_t>(match[4].str());
                colour.green = convert<uint8_t>(match[5].str());
                colour.blue = convert<uint8_t>(match[6].str());
            }
            else
            {
                colour = getColourFromString(match[7].str());
            }
#ifdef DEBUG_OUTPUT_REGEX
            for (auto const & i : match)
            {
                std::cout << (i.str().empty() ? "\"\"" : i.str()) << std::endl;
            }
#endif
            return {std::make_unique<Circle>(radius, colour), origin_x, origin_y};
        }
        catch (std::out_of_range & e)
        {
            throw std::runtime_error("Input data is not properly formatted:\n" + this->buf + "\n" + e.what());
        }
    }

    if (std::regex_match(shape_description.c_str(), match, triangle_regex))
    {
        try
        {
            auto x1 = convert<int32_t>(match[1].str());
            auto y1 = convert<int32_t>(match[2].str());
            auto x2 = convert<int32_t>(match[3].str());
            auto y2 = convert<int32_t>(match[4].str());

            auto origin_x = convert<int32_t>(match[5].str());
            auto origin_y = convert<int32_t>(match[6].str());

            Colored colour;

            if (match[7].matched)
            {
                colour.red = convert<uint8_t>(match[7].str());
                colour.green = convert<uint8_t>(match[8].str());
                colour.blue = convert<uint8_t>(match[9].str());
            }
            else
            {
                colour = getColourFromString(match[10].str());
            }
#ifdef DEBUG_OUTPUT_REGEX
            for (auto const & i : match)
            {
                std::cout << (i.str().empty() ? "\"\"" : i.str()) << std::endl;
            }
#endif
            return {std::make_unique<Triangle>(x1, y1, x2, y2, colour), origin_x, origin_y};
        }
        catch (std::out_of_range & e)
        {
            throw std::runtime_error("Input data is not properly formatted:\n" + this->buf + "\n" + e.what());
        }
    }

    if (std::regex_match(shape_description.c_str(), match, rectangle_regex))
    {
        try
        {
            auto width = convert<uint32_t>(match[1].str());
            auto height = convert<uint32_t>(match[2].str());

            auto origin_x = convert<int32_t>(match[3].str());
            auto origin_y = convert<int32_t>(match[4].str());

            Colored colour;

            if (match[5].matched)
            {
                colour.red = convert<uint8_t>(match[5].str());
                colour.green = convert<uint8_t>(match[6].str());
                colour.blue = convert<uint8_t>(match[7].str());
            }
            else
            {
                colour = getColourFromString(match[8].str());
            }
#ifdef DEBUG_OUTPUT_REGEX
            for (auto const & i : match)
            {
                std::cout << (i.str().empty() ? "\"\"" : i.str()) << std::endl;
            }
#endif
            return {std::make_unique<Rectangle>(width, height, colour), origin_x, origin_y};
        }
        catch (std::out_of_range & e)
        {
            throw std::runtime_error("Input data is not properly formatted:\n" + this->buf + "\n" + e.what());
        }
    }

    throw std::runtime_error("Input data is not properly formatted:\n" + this->buf);
}