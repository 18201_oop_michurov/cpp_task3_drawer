#include <iostream>
#include "shape_container.hpp"


ShapeContainer::ShapeContainer(
        std::unique_ptr<Shape> && shape_ptr,
        int32_t x0,
        int32_t y0)
        :
        shape_ptr(std::move(shape_ptr)),
        x0(x0),
        y0(y0)
{}


Shape const & ShapeContainer::getShape() const
{
    return *this->shape_ptr;
}


bool ShapeContainer::empty() const
{
    return this->shape_ptr == nullptr;
}
