#ifndef DRAWER_SHAPE_CONTAINER_HPP
#define DRAWER_SHAPE_CONTAINER_HPP


#include <memory>
#include "shape_parser_declaration.hpp"
#include "../../shape/shape.hpp"


class ShapeContainer
{
    friend class shape_parser::parser;

private:
    ShapeContainer(
            std::unique_ptr<Shape> && shape_ptr,
            int32_t x0,
            int32_t y0);

public:
    ShapeContainer() = delete;
    ShapeContainer(ShapeContainer const & other) = delete;
    ShapeContainer & operator=(ShapeContainer const & other) = delete;

    ShapeContainer(ShapeContainer && other) noexcept = default;
    ShapeContainer & operator=(ShapeContainer && other) noexcept = default;

    ~ShapeContainer() = default;

    Shape const & getShape() const;

    bool empty() const;

private:
    std::unique_ptr<Shape> shape_ptr;

public:
    int32_t x0;
    int32_t y0;
};


#endif //DRAWER_SHAPE_CONTAINER_HPP
