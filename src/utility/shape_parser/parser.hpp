#ifndef DRAWER_PARSER_HPP
#define DRAWER_PARSER_HPP


#include <istream>


#include "shape_parser_declaration.hpp"
#include "shape_container.hpp"
#include "../../shape/shape.hpp"


namespace shape_parser
{
    std::pair<uint32_t, uint32_t> getImageResolution(std::string const & string);

    Colored getColourFromString(std::string const & string);

    template <typename IntegralType>
    IntegralType convert(std::string const & string);

    class parser
    {
    public:
        class iterator;

        iterator begin();

        iterator end();

        explicit parser(std::istream & input);

        parser() = delete;

        parser(parser const & other) = delete;
        parser(parser && other) = default;

        parser & operator=(parser const & other) = delete;

    private:
        std::istream & is;
    };

    class parser::iterator : public std::iterator<std::input_iterator_tag, ShapeContainer>
    {
        friend parser;

    private:
        iterator(
                std::istream & input,
                bool eof);

    public:
        iterator() = delete;
        iterator & operator=(iterator const & other) = delete;
        iterator(iterator const & other) = default;

        bool operator!=(iterator const & other) const;

        ShapeContainer operator*() const;

        iterator & operator++();

    private:
        std::istream & is;
        std::string buf;
        bool eof;
    };
}


#endif //DRAWER_PARSER_HPP
