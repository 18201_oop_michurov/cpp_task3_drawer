#include <getopt.h>
#include <sstream>


#include "parser.hpp"


GlobalArgs_t arg_parser::parse_arguments(
        int argc,
        char ** argv,
        char const * opt_string)
{
    GlobalArgs_t global_args;

    std::stringstream err_message;

    int arg;

    opterr = 0;

    while ((arg = getopt(argc, argv, opt_string)) != -1)
    {
        switch (arg)
        {
            case 'o':
                global_args.output_file = optarg;

                break;

            case ':':
                err_message << "Error: -" << static_cast<char>(optopt) << std::endl
                            << "Option is missing an argument" << std::endl;

                throw std::runtime_error(err_message.str());

            case '?':
            default:
                err_message << "Error: -" << static_cast<char>(optopt) << std::endl
                            << "Unknown option" << std::endl;

                throw std::runtime_error(err_message.str());
        }
    }

    if (optind < argc)
    {
        global_args.input_file = argv[optind];
    }

    return global_args;
}
