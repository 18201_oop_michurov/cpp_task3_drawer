#ifndef CPP_TASK1_PHRASES_PARSER_H
#define CPP_TASK1_PHRASES_PARSER_H


#include <iostream>


struct GlobalArgs_t
{
    std::string input_file;
    std::string output_file;

    GlobalArgs_t() = default;
};


namespace arg_parser
{
    GlobalArgs_t parse_arguments(
            int argc,
            char ** argv,
            char const * opt_string);

}


#endif //CPP_TASK1_PHRASES_PARSER_H
