#ifndef DRAWER_UTILITY_FUNCTIONS_HPP
#define DRAWER_UTILITY_FUNCTIONS_HPP


#include <iostream>


#include "../shape/shape.hpp"


void printNames(
        Named const & named);


template <typename... Args>
void printNames(
        Named const & named,
        Args... args)
{
    std::cout << named.getName() << " ";

    printNames(args...);
}


void printNames(
        std::ostream & ostream,
        Named const & named);


template <typename... Args>
void printNames(
        std::ostream & ostream,
        Named const & named,
        Args... args)
{
    ostream << named.getName() << " ";

    printNames(ostream, args...);
}


double totalArea(
        Shape const & shape);


template <typename... Args>
double totalArea(
        Shape const & shape,
        Args... args)
{
    return shape.area() + totalArea(args...);
}


#endif //DRAWER_UTILITY_FUNCTIONS_HPP
