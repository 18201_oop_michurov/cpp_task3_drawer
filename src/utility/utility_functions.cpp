#include "utility_functions.hpp"


void printNames(
        Named const & named)
{
    std::cout << named.getName() << std::endl;
}


void printNames(
        std::ostream & ostream,
        Named const & named)
{
    ostream << named.getName() << std::endl;
}


double totalArea(
        Shape const & shape)
{
    return shape.area();
}